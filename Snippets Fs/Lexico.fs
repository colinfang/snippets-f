﻿module Itertools.Lexico
open System

let inline swap (myArray:_[]) i j =
    let tmp = myArray.[i]
    myArray.[i] <- myArray.[j]
    myArray.[j] <- tmp

let inline reverse (myArray:_[]) i j =
    let mutable a = i
    let mutable b = j
    while a < b do
        swap myArray a b
        a <- a + 1
        b <- b - 1

/// Algorithm L - Knuth TAOCP 4A 7.2.1.2
let inline nextLex (myArray: _[]) =
    // Search for the pivot starting from index i towards left.
    // i cannot be the last index of the array.
    let rec findPivot i =
        if i = -1 then -1
        elif myArray.[i] < myArray.[i + 1] then i
        else findPivot (i - 1)
    // From right to left, find the first element that is larger than the pivot.
    let rec findTarget value i =
        if (myArray.[i] > value) then i
        else findTarget value (i - 1)

    // L2: Find j - elements on the right are in descending order (e.g. 5,4,4,3,3)
    // It means the permutation cannot be increased without changing aj.
    let pivot = findPivot (myArray.Length - 2)
    if pivot = -1 then false
    else
        // L3: Increase aj - swapped by the smallest element (ak > aj) on the right.
        // Search from right to left.
        // (Search from left to right is slower because it takes an extra check on the right as well as checks end of array.)
        let target = findTarget myArray.[pivot] (myArray.Length - 1) 
        swap myArray pivot target
        // L4: Reverse
        reverse myArray (pivot + 1) (myArray.Length - 1)
        true

/// Requires N >= 3.
/// Algorithm L - Knuth TAOCP 4A 7.2.1.2 Ex 1
let inline nextLexA (myArray: _[]) =
    // Search for the pivot starting from index i towards left.
    // i cannot be the last index of the array.
    let rec findPivot i =
        if i = -1 then -1
        elif myArray.[i] < myArray.[i + 1] then i
        else findPivot (i - 1)
    // From right to left, find the first element that is larger than the pivot.     
    let rec findTarget value i =
        if (myArray.[i] > value) then i
        else findTarget value (i - 1)

    // [|... x,y,z|]
    let y = myArray.[myArray.Length - 2]
    let z = myArray.[myArray.Length - 1]

    if y < z then
        // L2: Easiest case - if y is the pivot, need N >= 2.
        // N!/2 cases that y < z
        myArray.[myArray.Length - 1] <- y
        myArray.[myArray.Length - 2] <- z
        true
    else
        let x = myArray.[myArray.Length - 3]
        if x < y then
            // L2.1: Next easiest case - if x is the pivot, need N >= 3.
            // N!/2/2 cases that hit here.
            if x < z then
                myArray.[myArray.Length - 1] <- y
                myArray.[myArray.Length - 2] <- x
                myArray.[myArray.Length - 3] <- z
            else
                myArray.[myArray.Length - 1] <- x
                myArray.[myArray.Length - 2] <- z
                myArray.[myArray.Length - 3] <- y
            true
        else
            // L2.2: Find j
            let pivot = findPivot (myArray.Length - 4)
            if pivot = -1 then false
            else
                // [|...y<x>=,.>=.,>=z|]
                let y = myArray.[pivot]
                let x = myArray.[pivot + 1]
                    
                if y < z then
                    // if target is z
                    // L3: Easy increase
                    myArray.[pivot]              <- z
                    myArray.[pivot + 1]          <- y
                    myArray.[myArray.Length - 1] <- x
                else
                    // L3.1: Increase aj
                    // Search from right to left .
                    // (Search from left to right is slower because it takes an extra check on the right as well as checks end of array.)
                    // skip z
                    let target = findTarget y (myArray.Length - 2)
                    myArray.[pivot]              <- myArray.[target]
                    myArray.[target]             <- y
                    // L4: Begin to reverse
                    // z is still myArray.[myArray.Length - 1]
                    myArray.[myArray.Length - 1] <- myArray.[pivot + 1]
                    myArray.[pivot + 1]          <- z
                // L4.1: Reverse the rest
                reverse myArray (pivot + 2) (myArray.Length - 2)
                true

/// Requires N >= 3.
/// Algorithm L - Knuth TAOCP 4A 7.2.1.2 Ex 1
/// Minor modification: use binary search for target.
let inline nextLexB (myArray: _[]) =
    // Search for the pivot starting from index i towards left.
    // i cannot be the last index of the array.
    let rec findPivot i =
        if i = -1 then -1
        elif myArray.[i] < myArray.[i + 1] then i
        else findPivot (i - 1)
    // From right to left, find the first element that is larger than the pivot.    
    let rec findTarget value i =
        if (myArray.[i] > value) then i
        else findTarget value (i - 1)

    // [|... x,y,z|]
    let y = myArray.[myArray.Length - 2]
    let z = myArray.[myArray.Length - 1]

    if y < z then
        // L2: Easiest case - if y is the pivot, need N >= 2.
        // N!/2 cases that y < z
        myArray.[myArray.Length - 1] <- y
        myArray.[myArray.Length - 2] <- z
        true
    else
        let x = myArray.[myArray.Length - 3]
        if x < y then
            // L2.1: Next easiest case - if x is the pivot, need N >= 3.
            // N!/2/2 cases that hit here.
            if x < z then
                myArray.[myArray.Length - 1] <- y
                myArray.[myArray.Length - 2] <- x
                myArray.[myArray.Length - 3] <- z
            else
                myArray.[myArray.Length - 1] <- x
                myArray.[myArray.Length - 2] <- z
                myArray.[myArray.Length - 3] <- y
            true
        else
            // L2.2: Find j
            let pivot = findPivot (myArray.Length - 4)
            if pivot = -1 then false
            else
                // [|...y<x>=,.>=.,>=z|]
                let y = myArray.[pivot]
                let x = myArray.[pivot + 1]
                    
                if y < z then
                    // if target is z
                    // L3: Easy increase
                    myArray.[pivot]              <- z
                    myArray.[pivot + 1]          <- y
                    myArray.[myArray.Length - 1] <- x
                else
                    // L3.1: Increase aj
                    // Below is the only change from nextLexA.
                    // Since the elements are ordered, check mid first.         
                    let target =
                        // Don't binary search if not many.
                        if myArray.Length - pivot < 5 then
                            findTarget y (myArray.Length - 2)
                        else
                            let mid = (pivot + myArray.Length - 1) / 2 
                            if myArray.[mid] > y then
                                // from right to left, skip z
                                // (Search from left to right is slower because it takes an extra check on the right.)
                                findTarget y (myArray.Length - 2)
                            else
                                // target is not mid
                                // from right to left
                                findTarget y (mid - 1)

                    myArray.[pivot]              <- myArray.[target]
                    myArray.[target]             <- y
                    // L4: Begin to reverse
                    // z is still myArray.[myArray.Length - 1]
                    myArray.[myArray.Length - 1] <- myArray.[pivot + 1]
                    myArray.[pivot + 1]          <- z
                // L4.1: Reverse the rest
                reverse myArray (pivot + 2) (myArray.Length - 2)
                true
