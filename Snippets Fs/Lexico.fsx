﻿#time "on"
#load "Lexico.fs"
open Itertools.Lexico

let inline permutateLexs myArray =
    let mutable d = 1
    Array.sortInPlace myArray
    printfn "first permutation: %A" myArray
    while nextLex myArray do
        d <- d + 1
    printfn "last permutation: %A" myArray
    printfn "Total permutations: %d" d

let inline permutateLexA myArray =
    let mutable d = 1
    Array.sortInPlace myArray
    printfn "first permutation: %A" myArray
    while nextLexA myArray do
        d <- d + 1
    printfn "last permutation: %A" myArray
    printfn "Total permutations: %d" d

let inline permutateLexB myArray =
    let mutable d = 1
    Array.sortInPlace myArray
    printfn "first permutation: %A" myArray
    while nextLexB myArray do
        d <- d + 1
    printfn "last permutation: %A" myArray
    printfn "Total permutations: %d" d

let myArray = [|12..-1..1|]
// Core i5-3450 3.10 GHz, Windows 8 64-bit, Visual Studio 2013, 32-bit Interactive.
permutateLexs myArray // 5.5s
permutateLexA myArray // 3.2s
permutateLexB myArray // 3.3s
